const std = @import("std");

fn copy_directory_recursive(src: []const u8, dst: []const u8) !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer std.debug.assert(gpa.deinit() == .ok);
    const allocator = gpa.allocator();

    var src_dir = try std.fs.cwd().openDir(src, .{ .iterate = true, });
    defer src_dir.close();

    var dest_dir = try std.fs.cwd().makeOpenPath(dst, .{});
    defer dest_dir.close();

    var walker = try src_dir.walk(allocator);
    defer walker.deinit();

    while (try walker.next()) |entry| {
        switch (entry.kind) {
            .file => {
                try entry.dir.copyFile(entry.basename, dest_dir, entry.path, .{});
            },
            .directory => {
                dest_dir.makeDir(entry.path) catch |err| switch (err) {
                    error.PathAlreadyExists => {}, // not an error
                    else => return err,
                };
            },
            else => return error.UnexpectedEntryKind,
        }
    }
}

// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) !void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    _ = b.standardTargetOptions(.{});

    // Standard optimization options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall. Here we do not
    // set a preferred release mode, allowing the user to decide how to optimize.
    _ = b.standardOptimizeOption(.{});

    const imgui_dep = b.dependency("imgui", .{});
    const cimgui_dep = b.dependency("cimgui", .{});

    // use system lua if available to run the cimgui generator script
    const lua_path: ?[]const u8 = b.findProgram(&.{ "luajit", "lua5.1" }, &.{})
        catch |err| switch (err) {
            error.FileNotFound => null,
            else => return err,
        };

    // hopefully, this can be replaced by a rewrite in zig in the future, until
    // then, python is necessary to generate the bindings
    const python_path = blk: {
        const path = b.findProgram(&.{ "python", "python3" }, &.{})
            catch |err| switch (err) {
                error.FileNotFound => return error.Python3NotFound,
                else => return err,
            };

        const result = try std.process.Child.run(.{
            .allocator = b.allocator,
            .argv = &.{
                path,
                "--version",
            },
            .max_output_bytes = 4096,
        });

        switch (result.term) {
            .Exited => |e| if (e != 0) return error.PythonUnexpectedError,
            else => unreachable,
        }
        break :blk path;
    };

    const lua51_dep: ?*std.Build.Dependency = blk: {
        if (lua_path == null) {
            break :blk b.lazyDependency("lua51", .{
                .target = b.host,
                .optimize = .ReleaseFast,
            });
        }

        break :blk null;
    };

    const cimgui_cache_clone = try std.fs.path.relative(
        b.allocator, 
        b.build_root.path orelse return error.InvalidBuildRoot, 
        try b.cache_root.join(b.allocator, &.{"tmp/cimgui"}),
    );
    try copy_directory_recursive(cimgui_dep.path("").getPath(b), cimgui_cache_clone);

    const cimgui_generate_command =
        if (lua_path) |path|
            b.addSystemCommand(&.{ path })
        else if (lua51_dep) |dep|
            b.addRunArtifact(dep.artifact("lua5.1"))
        else
            b.addSystemCommand(&.{ "luajit" });

    cimgui_generate_command.setCwd(b.path(b.pathJoin(&.{ cimgui_cache_clone, "generator/"})));
    cimgui_generate_command.addFileArg(b.path(b.pathJoin(&.{ cimgui_cache_clone, "generator/generator.lua"})));
    cimgui_generate_command.addArgs(&.{
        b.fmt("{s} cc", .{ b.graph.zig_exe }),
        "freetype",
        "-DIMGUI_ENABLE_STB_TRUETYPE -DIMGUI_USE_WCHAR32",
    });

    {
        const imgui_path = try std.fs.path.relative(
            b.allocator,
            b.pathJoin(&.{ cimgui_cache_clone, "generator/"}),
            imgui_dep.path("").getPath(b),
        );
        defer b.allocator.free(imgui_path);

        cimgui_generate_command.setEnvironmentVariable("IMGUI_PATH", imgui_path);
    }

    const fix_tool = b.addExecutable(.{
        .name = "fix_cimgui_sources",
        .root_source_file = b.path("src/fixup_generated_cimgui.zig"),
        .target = b.host,
    });
    const fix_step = b.addRunArtifact(fix_tool);
    fix_step.step.dependOn(&cimgui_generate_command.step);
    fix_step.addFileArg(b.path(cimgui_cache_clone));

    const write_step = b.addWriteFiles();
    write_step.step.dependOn(&fix_step.step);
    write_step.addCopyFileToSource(
        b.path(b.pathJoin(&.{ cimgui_cache_clone, "cimgui.cpp"})),
        "../src/generated/cimgui.cpp"
    );
    write_step.addCopyFileToSource(
        b.path(b.pathJoin(&.{ cimgui_cache_clone, "cimgui.h"})),
        "../src/generated/cimgui.h"
    );

    const python_generate_command = b.addSystemCommand(&.{ python_path });
    python_generate_command.step.dependOn(&write_step.step);
    python_generate_command.setEnvironmentVariable("PYTHONDONTWRITEBYTECODE", "1");
    python_generate_command.addFileArg(b.path("generate.py"));

    python_generate_command.setEnvironmentVariable(
        "COMMANDS_JSON_FILE",
        b.pathJoin(&.{ cimgui_cache_clone, "generator/output/definitions.json" }),
    );
    python_generate_command.setEnvironmentVariable(
        "IMPL_JSON_FILE",
        b.pathJoin(&.{ cimgui_cache_clone, "generator/output/definitions_impl.json" }),
    );
    python_generate_command.setEnvironmentVariable(
        "STRUCT_JSON_FILE",
        b.pathJoin(&.{ cimgui_cache_clone, "generator/output/structs_and_enums.json" }),
    );
    python_generate_command.setEnvironmentVariable(
        "TYPEDEFS_JSON_FILE",
        b.pathJoin(&.{ cimgui_cache_clone, "generator/output/typedefs_dict.json" })
    );

    {
        const template_file_relpath = try std.fs.path.relative(
            b.allocator,
            b.build_root.path orelse return error.InvalidBuildRoot,
            b.path("src/template.zig").getPath(b),
        );
        defer b.allocator.free(template_file_relpath);

        python_generate_command.setEnvironmentVariable(
            "TEMPLATE_FILE",
            template_file_relpath,
        );
    }

    {
        const output_relpath = try std.fs.path.relative(
            b.allocator,
            b.build_root.path orelse return error.InvalidBuildRoot,
            b.pathFromRoot("../src/generated/imgui.zig"),
        );
        defer b.allocator.free(output_relpath);

        python_generate_command.setEnvironmentVariable(
            "OUTPUT_PATH",
            output_relpath,
        );
    }

    b.getInstallStep().dependOn(&python_generate_command.step);
}
